import React from 'react';

const Card = ({ robot }) => {
    const { id, name } = robot;

    return (
        <div className="tc bg-hot-pink dib br3 pa3 ma2 grow bw shadow-5">
            <img src={`https://robohash.org/${id}?size=200x200`} alt="robots" />
            <div>
                <h2>{name}</h2>
            </div>
        </div>
    );
};

export default Card;