import React from 'react';
import Card from './Card';

const Cardlist = ({ robots }) => <div>
    {robots.map(robot => <Card key={robot.id} robot={robot} />)}
</div>;

export default Cardlist;