import React from 'react';

const Scroll = props => (
        <div style={{ overflow: 'scroll', border: 'none', height: '800px'}}>
            {props.children} 
        </div>
    );
    
export default Scroll;    